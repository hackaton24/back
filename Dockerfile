FROM node:16-alpine

ADD . /back/

WORKDIR /back

RUN npm install

CMD npm run start